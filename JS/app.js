// перше завдання
let product = {
    name: 'Lays',
    price: 100,
    discount: 15,
    priceAfterDiscount: function () {
        return this.price - (this.price / 100 * this.discount)
    }

};
console.log(product.priceAfterDiscount()); 



// друге завдання
function greeting(){
    return `Привіт, мені ${person.age} років.`;
}

const userName = prompt("Введіть ваше ім'я:");
const age = prompt("Введіть ваш вік:");

let person = {
    name: userName,
    age,
};

alert(greeting(person))

// третє завдання

function deepClone(obj) {
    if (typeof obj !== 'object' || obj === null) {
        return obj;
    }

    let clone = Array.isArray(obj) ? [] : {};

    for (let key in obj) {
        clone[key] = deepClone(obj[key]);
    }

    return clone;
}

const originalObject = {
    name: 'Vitalii',
    age: 20,
};

console.log(deepClone(originalObject));
